package com.example.td.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.td.R;

import org.w3c.dom.Text;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.Scorrect;
import static com.example.td.ui.MenuActivity.Sincorrect;
import static com.example.td.ui.MenuActivity.correct;
import static com.example.td.ui.MenuActivity.incorrect;

/**
 * Created by Андрей on 12.05.2017.
 */

public class Profile extends AppCompatActivity implements View.OnClickListener {
    TextView Correct;
    TextView Winrate;
    String Swinrate;
    int winrate;
    int allgames;
    RelativeLayout background;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        Correct = (TextView) findViewById(R.id.correct);
        Winrate = (TextView) findViewById(R.id.winrate);
        background = (RelativeLayout) findViewById(R.id.profile_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
        allgames = correct+incorrect;
        if (allgames==0){
            winrate=0;
        }
        else {
            winrate =correct/allgames*100;
        }
        Swinrate = getString(R.string.winrate);
        Correct.setText("Правильных ответов: " + Integer.toString(correct));
        Winrate.setText(Swinrate+ Integer.toString(winrate) + "%");

}

    @Override
    public void onClick(View view) {

    }
}