package com.example.td.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.td.R;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;

import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.Scorrect;
import static com.example.td.ui.MenuActivity.Sincorrect;
import static com.example.td.ui.MenuActivity.Spoints;
import static com.example.td.ui.MenuActivity.correct;
import static com.example.td.ui.MenuActivity.incorrect;
import static com.example.td.ui.MenuActivity.points;

/**
 * Created by Андрей on 07.02.2017.
 */

public class Settings extends AppCompatActivity implements View.OnClickListener {

    Button reset;
    RelativeLayout background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        background = (RelativeLayout) findViewById(R.id.settings_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }

        reset = (Button) findViewById(R.id.reset);
        reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.reset:


                points = 0;
                correct = 0;
                incorrect = 0;
                BackgroundColor = 0;
                POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);;
                SharedPreferences.Editor ed = POINTS.edit();
                ed.putInt(Spoints, points);
                ed.putInt(Scorrect, correct);
                ed.putInt(Sincorrect, incorrect);
                ed.putInt(SBackgroundColor, BackgroundColor);
                ed.apply();
                Intent q = new Intent(Settings.this, MenuActivity.class);
                startActivity(q);

                Toast.makeText(this, Integer.toString(points) , Toast.LENGTH_SHORT).show();


                break;
        }
    }
}